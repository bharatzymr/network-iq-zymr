#!/usr/bin/python

from nwiq import nwiq

class log_geo_mmdb_mdata:

	def __init__(self):
		print("Instantiate Class")
		self.ins_nwiq = nwiq()

	def log_geo_mmdb_mdata(self):
		try:
			#First call the geo function to specify path to mmdb file
			self.ins_nwiq.geo('/home/ubuntu/network-iq-zymr/GeoIP2-ISP_20170613/GeoLite2-City.mmdb')
			#Fetch all new data in NWIQ from the last 30 minutes
			self.ins_nwiq.db_connect("nwiq-python.cbmwsbw6g8jt.us-west-2.rds.amazonaws.com", "attack-job-server", "root", "IYHevDPLuvYMTJ2G")
			new_data = list(self.ins_nwiq.db_fetch_query_execute("select distinct(d.data_key), d.data_val from data d, data_audit a, jobs j where j.extract_type='ip_v4' and d.data_key = a.data_key and j.job_key=d.job_key and a.creation_date > date_sub(now(), interval 1 hour) limit 50000;"))

			for i in range(len(new_data)):
				#Call the log_isp_org_metadata function on each data_key, value pair
				try:
					self.ins_nwiq.log_geo_metaData(new_data[i][1], new_data[i][0])
				except:
					print("Geo data error - probably not in file")
					continue

		except:
			print("Error inserting Geo data")
			raise

if __name__ == "__main__":
	ins_log_geo_mmdb_mdata = log_geo_mmdb_mdata()
	ins_log_geo_mmdb_mdata.log_geo_mmdb_mdata()


