#!/usr/bin/python

from nwiq import nwiq
import json
import pandas as pd

class create_goodbots:


    def __init__(self):
        print("Instantiate Class")
        self.ins_nwiq = nwiq()

    def fdrec(self, df):
        drec = dict()
        ncols = df.values.shape[1]
        for line in df.values:
            d = drec
            for j, col in enumerate(line[:-1]):
                if col not in d.keys():
                    if j != ncols - 2:
                        d[col] = {}
                        d = d[col]
                    else:
                        d[col] = line[-1]
                else:
                    if j != ncols - 2:
                        d = d[col]
        return drec

    def create_goodbots_json(self):

        try:
            self.ins_nwiq.db_connect("nwiq-python.cbmwsbw6g8jt.us-west-2.rds.amazonaws.com", "attack-job-server",
                                     "root", "IYHevDPLuvYMTJ2G")
            gb_list = list(self.ins_nwiq.db_fetch_query_execute("select * from stealth_good_bots_data;"))
            ua_list = [el[3] for el in gb_list]
            org_list = [el[4] for el in gb_list]
            ip_list = [el[5] for el in gb_list]
            name_list = [el[1] for el in gb_list]
            parsed_family_list = [el[2] for el in gb_list]
            gb_df = pd.DataFrame(data={'UA_string': ua_list, 'Org': org_list, 'IP': ip_list, 'Bot_name': name_list,'Parsed_family':parsed_family_list})[['Parsed_family', 'UA_string', 'Org', 'IP', 'Bot_name']]
            final_dict = self.fdrec(gb_df)
            with open('/home/ubuntu/attack-db/nwiq_goodbots_list.json', 'w') as file:
                json.dump(final_dict, file)

        except:
            print("Error creating good bots list")
            raise

if __name__ == "__main__":
    ins_create_goodbots = create_goodbots()
    ins_create_goodbots.create_goodbots_json()

