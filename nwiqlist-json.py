#!/usr/bin/python

from nwiq import nwiq
import csv
import json

class create_json:

	def __init__(self):
		print("Instantiate Class")
		self.ins_nwiq = nwiq()
	
	def create_json_file(self):
		try:
			#self.ins_nwiq.db_connect("10.3.29.58", "attack-job-server", "root", "BMvw4B3eBBZTSApEvhNV")
			with open('/home/ubuntu/attack-db/lists/nwiqv3.txt', 'r') as file:
				reader = csv.reader(file)
				nwiq_list = list(reader)

			test_dict = {}
			count = 1
			all_rule_dict = {}
			for i in range(len(nwiq_list)):
				if nwiq_list[i][7] in test_dict:
					continue
				else:
					test_dict[nwiq_list[i][7]]=1

				rule_dict = {'enabled':'true',
						'namespace':'Rules.BaseData',
                				'alert_name':nwiq_list[i][7],
                				'category':'Suspicious Source',
                				'rule_hash_component':'true',
                				'description':'IPs found in '+str(nwiq_list[i][7])+' Blacklists',
                				'experimental':'true',
                				'hidden':'true',
                				'weight':0,
                				'rule_expression':'input.get_netiq_labels().contains(\"'+nwiq_list[i][7]+'\")'}
				all_rule_dict['N'+str(count)] = rule_dict
				count += 1

			final_dict = {}
			final_dict['Name'] = 'System'
			final_dict['rules'] = all_rule_dict

			with open('/home/ubuntu/attack-db/lists/nwiqv3.json', 'w') as outfile:
				json.dump(final_dict, outfile)


		except:
			print("Error Creating JSON")
			raise

if __name__ == "__main__":
	ins_create_json = create_json()
	ins_create_json.create_json_file()




