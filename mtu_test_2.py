#!/usr/bin/python

from nwiq import nwiq
import subprocess

class mtu_test_2:

    def __init__(self):
        """
            @Description: Initialize the class and define objects
        """
        
        print("Instantiate Class")
        self.ins_nwiq = nwiq()
        print("Instantiate www definition")
        self.ins_nwiq.www()
        
    def fetch_ipv4_data(self):
        
        print("Executing select query")
        self.ins_nwiq.db_connect("nwiq-python.cbmwsbw6g8jt.us-west-2.rds.amazonaws.com", "attack-job-server", "root", "IYHevDPLuvYMTJ2G")
        db_query = "select data_val, data_key from data where data_type='ip_v4' \
        order by data_key desc limit 2000;"
        
        data_tuple = self.ins_nwiq.db_fetch_query_execute(db_query)
        print(len(data_tuple))
        try:
            for mtu_data in range(len(data_tuple)):
                print('Testing %s: %s\n'%(mtu_data, data_tuple[mtu_data][0]))
                mtu_value = 1478
                while 1:
                    if mtu_value > 0:
                        proc = subprocess.Popen(['timeout','3','ping', '-c', '1','-i','1','-M','do','-s','%s'%(mtu_value),'-w','1', data_tuple[mtu_data][0]], stdout=subprocess.PIPE)
                        status = proc.stdout.read()
                        print("Status: %s"%(status))
                        status = status.decode("utf-8")
                        lines = status.split('\n')
                        print("Lines: %s"%(lines))
                        for line in lines:
                            if "packets transmitted" in line:
                                expected_line = line
                        
                        if '1 errors' in expected_line:
                            print('%s too big\n'%(mtu_value))
                            mtu_value = mtu_value - 2
                        
                        elif len(lines) < 2:
                            print('MTU is %s\n'%(mtu_value))
                            print('ICMP blocked\n')
                            meta_response = self.ins_nwiq.insert_metadata(data_tuple[mtu_data][1], "MTU", mtu_value)
                            meta_response = self.ins_nwiq.insert_metadata(data_tuple[mtu_data][1], "ICMP", "blocked")
                            break
                        
                        elif mtu_value < 1300:
                            print('MTU is unknown\n')
                            print('ICMP is unknown\n')
                            meta_response = self.ins_nwiq.insert_metadata(data_tuple[mtu_data][1], "MTU", "unknown")
                            meta_response = self.ins_nwiq.insert_metadata(data_tuple[mtu_data][1], "ICMP", "unknown")
                            
                        else:
                            print('MTU is %s\n'%(mtu_value))
                            print('ICMP is open\n')
                            meta_response = self.ins_nwiq.insert_metadata(data_tuple[mtu_data][1], "MTU", mtu_value)
                            meta_response = self.ins_nwiq.insert_metadata(data_tuple[mtu_data][1], "ICMP", "open")
                            break
                    else:
                        print('MTU value is below or equal to zero. So existing loop')
                        break
        
        except:
            print("Error occurred while fetching data")
    
if __name__ == "__main__":
    ins_mtu_test = mtu_test_2()
    ins_mtu_test.fetch_ipv4_data()
