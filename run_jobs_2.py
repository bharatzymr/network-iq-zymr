#!/usr/bin/python

from nwiq import nwiq

class run_job_2:

    def __init__(self):
        """
            @Description: Initialize the class and define objects
        """
        
        print("Instantiate Class")
        self.ins_nwiq = nwiq()
        print("Instantiate www definition")
        self.ins_nwiq.www()
        
    def initiate_process_job(self):
        
        self.ins_nwiq.db_connect("nwiq-python.cbmwsbw6g8jt.us-west-2.rds.amazonaws.com", "attack-job-server", "root", "IYHevDPLuvYMTJ2G")
        job_list = self.ins_nwiq.get_job()
        if job_list != []:
            total_job_count = len(job_list)
            print('Total jobs to be executed: %s'%(total_job_count))
            remaining_job_count = total_job_count
            print(job_list)
            for job in job_list:
                print(job)
                try:
                    self.ins_nwiq.process_job(job)
                    remaining_job_count = remaining_job_count-1
                    print('Remaining Jobs to be executed: %s'%(remaining_job_count))
                except:
                    print('Error processing job')
                    raise
                
        else:
            print('Fetched job list is empty...')
    
if __name__ == "__main__":
    ins_run_job = run_job_2()
    ins_run_job.initiate_process_job()
