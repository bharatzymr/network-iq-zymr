#!/usr/bin/python

from nwiq import nwiq

class virus_total_2:

    def __init__(self):
        """
            @Description: Initialize the class and define objects
        """
        
        print("Instantiate Class")
        self.ins_nwiq = nwiq()
        print("Instantiate www definition")
        self.ins_nwiq.www()
        
    def virus_total_test(self):
        
        self.ins_nwiq.db_connect("nwiq-python.cbmwsbw6g8jt.us-west-2.rds.amazonaws.com", "attack-job-server", "root", "IYHevDPLuvYMTJ2G")
        
        for loop_count in range(4):
            print("Executing select query for %s round"%(loop_count+1))
            db_query_1 = "select data_key from data where creation_date > \
            date_sub(now(), interval 1 day) ORDER BY RAND() limit 1;"
            
            data_tuple_1 = self.ins_nwiq.db_fetch_query_execute(db_query_1)
            if len(data_tuple_1) > 0:
                print(data_tuple_1[0][0])
                
                db_query_2 = "select * from data where data_key=%s and \
                        data_type='ip_v4';"%(data_tuple_1[0][0])
                
                data_tuple_2 = self.ins_nwiq.db_fetch_query_execute(db_query_2)
                if len(data_tuple_2) > 0:
                    print(data_tuple_2)
                    
                    try:
                        virus_url = "https://www.virustotal.com/vtapi/v2/ip-address/report?ip=%s&apikey=d6a1a63a14d3488d66c29a7c8859d8907c2af7f474923e59deb875caffef9e25"%(data_tuple_2[0][3])
                        print(virus_url)
                        virus_response = self.ins_nwiq.get_url(virus_url)
                        if not virus_response.status_code == 204:
                            print("*****************")
                            print(virus_response.text)
                            print(type(virus_response.text))
                            virus_json_obj = self.ins_nwiq.decode_json(virus_response.text)
                            print(virus_json_obj)
                            print(type(virus_json_obj))
                            print("*****************")
                            
                            if 'verbose_msg' in virus_json_obj:
                                self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'verbose_msg',
                                                          virus_json_obj['verbose_msg'])
                            
                            if 'as_owner' in virus_json_obj:
                                self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'as_owner',
                                                          virus_json_obj['as_owner'])
                            
                            if 'country' in virus_json_obj:
                                self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'country',
                                                          virus_json_obj['country'])
                            
                            if 'asn' in virus_json_obj:
                                self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'asn',
                                                          virus_json_obj['asn'])
                            
                            if 'detected_urls' in virus_json_obj:
                                for obj_count in range(len(virus_json_obj['detected_urls'])):
                                    self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'v_url', 
                                                          virus_json_obj['detected_urls'][obj_count]['url'])
                                    
                                    self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'positives',
                                                          virus_json_obj['detected_urls'][obj_count]['positives'])
                                    
                                    self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'scan_date',
                                                          virus_json_obj['detected_urls'][obj_count]['scan_date'])
                            
                            if 'resolutions' in virus_json_obj:
                                for obj_count in range(len(virus_json_obj['resolutions'])):
                                    self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'last_resolved',
                                                          virus_json_obj['resolutions'][obj_count]['last_resolved'])
                                    
                                    self.ins_nwiq.insert_metadata(data_tuple_2[0][0],
                                                          'v_url',
                                                          virus_json_obj['resolutions'][obj_count]['hostname'])
                        
                        else:
                            print("Get request returned 204 response for %s round"%(loop_count))
                    except:
                        raise
                
                else:
                    time.sleep(4)
            
            else:
                print("Select query returned empty tuple for %s round..."%(loop_count))
            
if __name__ == "__main__":
    ins_virus_total = virus_total_2()
    ins_virus_total.virus_total_test()
