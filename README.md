# Stealth Perl To Python Migration

This provides migrated python scripts of existing perl scripts. Total 6 scripts to be migrated named as mtu_test_2, nwiq, parse_cidr_2, reverse_dns_2, run_jobs_2, virus_total_2.

## Getting Started

These instructions will assist you to install dependacies for running it on machine for development.

### Prerequisites

You will need to have Python3.6 installed.
To install other dependacies, you will need pip.

### Installing
Follow below steps:
```sh
pip3 install PyMySQL
```
```sh
pip3 install geoip2
```
```sh
pip3 install requests
```
```sh
pip3 install dnspython
```
```sh
pip3 install --upgrade ipwhois
```
```sh
pip3 install netaddr
```
```sh
sudo apt-get install python3-pandas
```
## Running the tests
Go to directory of these scripts and follow below steps:
- python3 run_jobs_2.py > run_jobs_2.log 2>&1.
- python3 mtu_test_2.py > mtu_test_2.log 2>&1.
- python3 parse_cidr_2.py > parse_cidr_2.log 2>&1.
- python3 reverse_dns_2.py > reverse_dns_2.log 2>&1.
- python3 virus_total_2.py > virus_total_2.log 2>&1.

