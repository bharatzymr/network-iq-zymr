#!/usr/bin/python
"""
Import Libraries
"""

import pymysql
import json
import csv
import requests
import cgi
import time
from ipwhois import IPWhois
import re
import pprint
from geoip2 import database as geoip2_database
from dns import resolver
import netaddr
from netaddr import IPNetwork
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class nwiq:
    """
    @Description: This class contain definitions for
        - database connections
        - various database query execution
    """
    
    def __init__(self):
        """
        @Description: Initialize the class and define objects
        """
        
        self.db_connection = None
        self.geoip_database = None
        self.request_obj = requests
        self.request_timeout = 30
        # This object covers perl method dns and rbl
        self.dns_obj = resolver
        # This object covers perl method cgi
        self.cgi_obj = cgi
        self.depth = 0
        self.url_list = {}
        self.count = 0
        self.jobrun = []
        self.www_handle = None
        self.request_headers = None
        self.pprint_obj = pprint.PrettyPrinter(indent=4)
        self.unique_content = {}
        
    def db_connect(self, database_host, database_name, 
                   database_username, database_password):
        """
        @Description: Method to connect to MySQL database
        Parameters:
                database_host = Database host to which connection need
                                to be made
                database_name = Database name to which connection need
                                to be Made
                database_username = Username using which database
                                    connection need to be made
                database_password = Password using which database
                                    connection need to be made
        Returns: It sets the connection object so further execution
                and raise exception if any
        Perl Method: This definition covers perl methods dbConnect 
                    and _dbconnect
        """
        
        try:
            # Open database connection
            print("Connecting to database.")
            self.db_connection = pymysql.connect(host=database_host, db=database_name,
                                                 user=database_username,
                                                 passwd=database_password)
            print('Successfully connected to database.')
        
        except:
            print('Error: Unable to connect to database. '
                  'Please check provided database details.')
            raise
    
    def db_update_query_execute(self, database_query):
        """
        @Description: Method to execute query to insert or update data
                        in MySQL database
        Parameters:
                database_query = Database query to be executed to insert or
                                update data in database
        Return: Returns key and raise exception if any
        Perl Method: This definition partially covers perl methods psql
        """
        
        try:
            print("Executing insert data query: %s"%(database_query))
            # prepare a cursor object using cursor() method
            db_cursor = self.db_connection.cursor()
            # Execute the SQL command
            db_cursor.execute(database_query)
            # Commit your changes in the database
            self.db_connection.commit()
            insert_key = self.db_connection.insert_id()
            return insert_key
            
        except:
            print("Error occurred while updating data %s. \
            Performing Rollback..."%(database_query))
            # Rollback in case there is any error
            self.db_connection.rollback()
            print("Performed Rollback successfully.")
            raise
    
    def db_fetch_query_execute(self, database_query):
        """
        @Description: Method to execute query to retrieve data
                        from MySQL database
        Parameters:
                database_query = Database query to be executed to retrieve data
                                from database
        Retuens:    Returns fetched data and raise exception if any
        Perl Method: This definition partially covers perl methods psql
        """
        
        try:
            print("Executing fetch data query: %s"%(database_query))
            # prepare a cursor object using cursor() method
            db_cursor = self.db_connection.cursor()
            # Execute the SQL command
            db_cursor.execute(database_query)
            # Commit your changes in the database
            fetched_data = db_cursor.fetchall()
            return fetched_data
        
        except:
            print('Error occurred while executing query: %s'%(database_query))
            raise
        
    def db_disconnect(self):
        """
        @Description: Method to close opened MySQL database connection
        Returns: raise exception if any
        """
        
        try:
            # disconnect from server
            self.db_connection.close()
        
        except:
            print("Error occurred while closing the database connection.")
            raise
    
    def geo(self, maxmind_filepath):
        """
        @Description: Method to open Maxmind file
        Parameters:
                maxmind_filepath = maxmind filepath
        Returns: database object and raise exception if any
        Perl Method: This definition covers perl method geo
        """
        
        if maxmind_filepath == "":
            raise SystemExit("Can't open Geo::IP database. "
                             "Please specify a path to the maxmind .dat file.")
        
        else:
            try:
                # This creates a Reader object.
                self.geoip_database = geoip2_database.Reader(maxmind_filepath)
            except:
                print("Error occurred while reading the maxmind file.")
                raise
    
    def encode_json(self, json_obj):
        """
        @Description: Method to encode the json data
        Parameters:
                json_obj = Json data to be encoded
        Returns: Encodes json data and raise exception if any
        Perl Method: This definition covers perl method encodeJson
                     json method of perl is handled by
                     python's json library itself 
        """
        
        try:
            # Use dumps method of json library to encode data
            json_encoded_data = json.dumps(json_obj)
        except:
            print("Error occurred while encoding the json.")
            raise
        
        return json_encoded_data
    
    def decode_json(self, json_obj):
        """
        @Description: Method to decodes the json data
        Parameters:
                json_obj = Json data to be decoded
        Returns: Decode jsnon data and raise exception if any
        Perl Method: This definition covers perl method decodeJson
                    json method of perl is handled by
                    python's json library itself
        """
        
        try:
            # Use loads method of json library to decode data
            json_decoded_data = json.loads(json_obj)
        except:
            print("Error occurred while decoding the json.")
            raise
        
        return json_decoded_data
    
    def www(self):
        """
        @Description: Method to invoke get-mech and return handle
        Perl Method: This definition covers perl method www
        """
        
        self.get_mech()
        
    def get_mech(self):
        """
        @Description: Method to configure the user agent and request
        Returns: Request handler to be used in www method
        Perl Method: This definition covers perl method getMech
        """
        
        user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 \
                        (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'
        
        self.request_headers = {'user-agent': '%s'%(user_agent)}
        
        return self.request_headers
        
    def get_url(self, url):
        """
        @Description: Method to perform get request to provided URL
                        and returns response in Json format
        Parameters:
                url = url to which get request need to be sent
        Returns: request response and raise exception if any
        Perl Method: This definition covers perl method get
        """
        
        try:
            print("Performing GET request")
            request_response = self.request_obj.get(url, timeout=self.request_timeout, headers=self.request_headers, verify=False)
            return request_response
        
        except:
            print("Error occurred while get request for specified URL.")
            raise
    
    def proxy(self, host_ip, host_port, host_proto):
        """
        @Description: Method to perform proxy request to provided
                        ip and port over specified protocol
        Parameters:
                host_ip = ip to which proxy request to be made
                host_port = port on which proxy request to be made
                host_proto = protocol using using proxy request to be made
        Perl Method: This definition covers perl method proxy
        """
        
        try:
            proxy_url = r'%s://%s:%s'%(host_proto, host_ip, host_port)
            self.request_obj.proxy(host_proto, proxy_url)
        except:
            print("Error occurred while proxy request for specified details.")
            raise
    
    def get_max_datakey(self):
        """
        @Description: Method to fetch max key from data table
        Returns: max data key
        Perl Method: This definition covers perl method getMaxDataKey
        """
        
        max_datakey = None
        max_datakey_query = 'select max(data_key) as max_key from data limit 1;'
        max_datakey_tuple = self.db_fetch_query_execute(max_datakey_query)
        if len(max_datakey_tuple) > 0:
            max_datakey = max_datakey_tuple[0][0]
        return max_datakey
        
    def get_min_datakey(self):
        """
        @Description: Method to fetch min key from data table
        Returns: min data key
        Perl Method: This definition covers perl method getMinDataKey
        """
        
        min_datakey = None
        min_datakey_query = 'select min(data_key) as min_key from data limit 1;'
        min_datakey_tuple = self.db_fetch_query_execute(min_datakey_query)
        if len(min_datakey_tuple) > 0:
            min_datakey = min_datakey_tuple[0][0]
        return min_datakey
    
    def now(self):
        """
        @Description: Method to fetch info from now
        Perl Method: This definition covers perl method now
        """
        
        now_value = None
        now_query = 'select now() as n;'
        now_tuple = self.db_fetch_query_execute(now_query)
        if len(now_tuple) > 0:
            now_value = now_tuple[0][0]
        return now_value
    
    def insert_data(self, data_type, data_source, data_val, data_key):
        """
        @Description: Method to execute query to insert data in data table
        Parameters:
                data_type = data type
                data_source = data source
                data_val = data value
                data_key = job key
        Returns: data key
        Perl Method: This definition covers perl method insertData
        """
        
        fetch_data_query = "select data_key from data where \
                        data_val='%s' and data_source='%s';"%(data_val,
                                                          data_source)
        fetch_data_tuple = self.db_fetch_query_execute(fetch_data_query)
        
        if len(fetch_data_tuple) > 0:
            self.insert_audit(fetch_data_tuple, data_source)
            fetched_data_key = fetch_data_tuple[0][0]
            return fetched_data_key
        else:
            #if data_type == 'ip_v4':
                # Remove any leading zeros from data_val string
            #    data_val = re.sub(r'0', '', data_val)
            insert_data_query = "insert into data (data_type, data_source, \
                                    data_val, job_key) values ('%s', '%s', \
                                    '%s', %s);"%(data_type, data_source,
                                               data_val, data_key)
            self.db_update_query_execute(insert_data_query)
            data_query = "select data_key from data where data_val='%s' and \
                            data_source='%s';"%(data_val, data_source)
            data_list = self.db_fetch_query_execute(data_query)
            self.insert_audit(data_list, data_source)
            returned_data_key = self.insert_data(data_type, data_source, data_val, data_key)
            return returned_data_key
    
    def insert_audit(self, test, source):
        """
        @Description: Method to execute query to insert data in 
                        data_audit table
        Parameters:
                test = data list
                source = data source
        Perl Method: This definition covers perl method insertAudit
        """
        
        audit_query = "select * from data_audit where data_key='%s' \
                        and source='%s' and creation_date > date_sub(now(), \
                        interval 4 hour);"%(test[0][0], source)
        audit_data_key_tuple = self.db_fetch_query_execute(audit_query)
        
        if len(audit_data_key_tuple) > 0:
            pass
        else:
            insert_audit_data_query = "insert into data_audit \
                (data_key, source) values (%s, '%s');"%(test[0][0], source)
            self.db_update_query_execute(insert_audit_data_query)
    
    def insert_metadata(self, data_key, mdata_type, mdata_value):
        """
        @Description: Method to execute query to insert data in metadata table
        Parameters:
                data_key = data key
                mdata_type = metadata type
                mdata_value = metadata value
        Returns: data key
        Perl Method: This definition covers perl method insertMetadata
        """
        
        mdata_key_query = "select mdata_key from metadata \
                            where data_key=%s and mdata_type='%s' \
                            and mdata_value='%s';"%(data_key,
                                                  mdata_type,
                                                  mdata_value)
        mdata_key_tuple = self.db_fetch_query_execute(mdata_key_query)
        
        if len(mdata_key_tuple) > 0:
            mdata_key = mdata_key_tuple[0][0]
            return mdata_key
        else:
            insert_mdata_query = "insert into metadata (data_key, \
                                        mdata_type, mdata_value) values \
                                        (%s, '%s', '%s');"%(data_key,
                                                        mdata_type,
                                                        mdata_value)
            self.db_update_query_execute(insert_mdata_query)
            metadata_key = self.insert_metadata(data_key,
                                                             mdata_type,
                                                             mdata_value)
            return metadata_key
    
    def insert_ip_cidr(self, data_key, cidr_key):
        """
        @Description: Method to execute query to insert data in ip_cidr table
        Parameters:
                data_key = data key
                cidr_key = cidr key
        Returns: data key
        Perl Method: This definition covers perl method insertIpCidr
        """
        
        cidr_query = "select ic_key from ip_cidr where \
                        data_key=%s and cidr_key=%s;"%(data_key, cidr_key)
        ic_key_tuple = self.db_fetch_query_execute(cidr_query)
        
        if len(ic_key_tuple) > 0:
            ic_key = ic_key_tuple[0][0]
            return ic_key
        else:
            ipcidr_query = "insert into ip_cidr (data_key, cidr_key) \
                                values (%s, %s);"%(data_key, cidr_key)
            self.db_update_query_execute(ipcidr_query)
            ipcidr_key = self.insert_ip_cidr(data_key, cidr_key)
            return ipcidr_key
    
    def insert_data_enrichment(self, data_key, der_key):
        """
        @Description: Method to execute query to insert data in 
                        data_enrichment table
        Parameters:
                data_key = data key
                der_key = der key
        Returns: data key
        Perl Method: This definition covers perl method insertDataEnrichment
        """
        
        de_key_query = "select de_key from data_enrichment \
                        where data_key=%s and der_key=%s;"%(data_key, der_key)
        de_key_tuple = self.db_fetch_query_execute(de_key_query)
        
        if len(de_key_tuple) > 0:
            de_key = de_key_tuple[0][0]
            return de_key
        else:
            data_enrich_query = "insert into data_enrichment \
                                    (data_key, der_key) values \
                                    (%s, %s);"%(data_key, der_key)
            self.db_update_query_execute(data_enrich_query)
            enrich_key = self.insert_data_enrichment(data_key,
                                                                  der_key)
            return enrich_key
    
    def insert_cred(self, username, head, value, cred_type):
        """
        @Description: Method to execute query to insert data in 
                        credentials table
        Parameters:
                username = username
                head = head
                value = value
                cred_type = cred type
                cred_key = cred key
        Returns: data key
        Perl Method: This definition covers perl method insertCred
        """
        
        cred_key_query = "select cred_key from credentials \
                            where username='%s';"%(username)
        cred_key_tuple = self.db_fetch_query_execute(cred_key_query)
        
        if len(cred_key_tuple) > 0:
            cred_key = cred_key_tuple[0][0]
        else:
            insert_username_query = "insert into credentials (username) \
                                        values ('%s');"%(username)
            self.db_update_query_execute(insert_username_query)
            cred_key_tuple = self.db_fetch_query_execute(cred_key_query)
            if len(cred_key_tuple) > 0:
                cred_key = cred_key_tuple[0][0]
                    
        cred_key_metadata_query = "select cred_key from credential_metadata \
                                    where cm_type='%s' and cm_value='%s' and \
                                    cred_key=%s and \
                                    cred_type_value='%s';"%(head, value, 
                                                          cred_key, cred_type)
        cred_key_metadata_tuple = self.db_fetch_query_execute(cred_key_metadata_query)
        if len(cred_key_metadata_tuple) > 0:
            cred_key = cred_key_metadata_tuple[0][0]
            return cred_key
        else:
            metadata_insert_query = "insert into credential_metadata \
                                    (cm_type, cm_value, cred_key, \
                                    cred_type_value) values \
                                    ('%s', '%s', %s, '%s');"%(head, value,
                                                        cred_key, cred_type)
            self.db_update_query_execute(metadata_insert_query)
            self.insert_cred(username, head, value, cred_type)
            
    def update_job_runtime(self, job_key, status):
        """
        @Description: Method to updates the last runtime for the job
        Parameters:
                job_key = job key
                status = job status
        Perl Method: This definition covers perl method updateJobRuntime
        """
        
        update_job_query = "update jobs set last_runtime=now() \
                            where job_key=%s;"%(job_key)
        insert_job_query = "insert into job_run_audit (job_key, status) \
                            values (%s, %s);"%(job_key, status)
        
        self.db_update_query_execute(update_job_query)
        self.db_update_query_execute(insert_job_query)
        
    def process_content(self, content):
        """
        @Description: Method to process the provided content
        Parameters:
                content = content to be processed
        Perl Method: This definition covers perl method processContent
        """
        
        content_keys = list(content.keys())
        for content_key in content_keys:
            content_type = content[content_key]['type']
            content_source = content[content_key]['source']
            content_job_key = content[content_key]['job_key']
            if content_type == 'creds':
                print("***CRED***")
                print(content_key)
                print("*********")
                continue
            else:
                insert_key = self.insert_data(content_type, content_source,
                                              content_key, content_job_key)
            
            if content_type == 'ip_v4':
                # Integer IP unpack logic
                content[content_key]['integer_ip'] = int(netaddr.IPAddress(content_key))
            
            content_detail_keys = list(content[content_key].keys())
            for content_detail_key in content_detail_keys:
                if content_detail_key not in ('type', 'source', 'job_key', ''):
                    self.insert_metadata(insert_key, content_detail_key,
                                         content[content_key][content_detail_key])
    
    def get_job_byID(self, key):
        """
        @Description: Method to fetch job details from jobs table
        Parameters:
                key = key passed to get_job_byID method
        Perl Method: This definition covers perl method getJobById
        """
        
        job_query = 'select * from jobs j, extract_regex e, exclude_regex ex, \
                    cleanup_regex cr where job_key=%s and \
                    j.exclude_regex=ex.exr_key and j.cleanup_regex=cr.c_key \
                    and j.extract_regex=e.reg_key;'%(key)
        
        job_detail_tuple = self.db_fetch_query_execute(job_query)
        #print('Printing job tuple in GetJObByID %s'%(job_detail_tuple))
        self.jobrun.append(job_detail_tuple[0])
        job_child_key = job_detail_tuple[0][13]
        if isinstance(job_child_key,str) == False and job_child_key != None and job_child_key > 0:
            time.sleep(2)
            self.get_job_byID(job_child_key)
    
    def queue_job_byID(self, key):
        """
        @Description: Method to invoke get_job_byID method
        Parameters:
                key = key passed to get_job_byID method
        Perl Method: This definition covers perl method queueJobById
        """
        self.get_job_byID(key)
        self.list_queued_jobs()
    
    def list_queued_jobs(self):
        """
        Perl Method: This definition covers perl method listQueuedJobs
        """
        pass
    
    def get_job(self):
        """
        @Description: Method to fetch job from jobs table
                and check if repeat value is 0 depending upon which
                update job list
        Returns: job list
        Perl Method: This definition covers perl method getJob
        """
        job_list = []
        job_query = 'select * from jobs j, extract_regex e, \
                        exclude_regex ex, cleanup_regex c where \
                        parent_key=0 and j.exclude_regex=ex.exr_key and \
                        j.cleanup_regex=c.c_key and j.extract_regex=e.reg_key and j.repeat=1 \
                        order by rand();'
        jobs_tuple = self.db_fetch_query_execute(job_query)
        if len(jobs_tuple) > 0:
            for job_count in range(len(jobs_tuple)):
                job_last_runtime = jobs_tuple[job_count][15]
                job_repeat_interval = jobs_tuple[job_count][5]
                new_job_query = "select * from jobs where date_add('%s', \
                            interval %s minute) < now() and \
                            parent_key=0 limit 1;"%(job_last_runtime,
                                                    job_repeat_interval)
                
                job_detail_tuple = self.db_fetch_query_execute(new_job_query)
                
                if len(job_detail_tuple) > 0:
                    print('%s, %s, %s'%(jobs_tuple[job_count][0],
                                job_detail_tuple[0][0],
                                job_last_runtime))
                    
                    if jobs_tuple[job_count][11] == 0:
                        #if re.search('2016-03-23', job_last_runtime):
                        #    print('This job hasnt run yet')
                        #else:
                        continue
                    
                    job_list.append(jobs_tuple[job_count])
        
        else:
            print("Select query returned empty tuple...")
        
        return job_list
        
    def run_jobs(self):
        """
        @Description: Method to invoke process_job method
        Perl Method: This definition covers perl method runJobs
        """
        
        self.pprint_obj(self.jobrun)
        for job_count in range(len(self.jobrun)):
            self.process_job(self.jobrun[job_count])
    
    def process_job(self, job):
        """
        @Description: Method to process job.
        Parameters:
                job = job to be processed
        Perl Method: This definition covers perl method processJob
        """
        
        print('JOB:\n %s'%(job,))
        if self.request_headers == None:
            self.www()
        
        self.count += 1
        if not self.count > 20:
            if self.depth < 1:
                self.jobrun.append(job)
            
            job_child_key = job[13]
            if isinstance(job_child_key,str) == False and job_child_key != None and job_child_key > 0:
                self.get_job_byID(job_child_key)
            
            print('Total Job count from JobRun: %s'%(len(self.jobrun)))
            print(self.jobrun[-1])
            print('Processing Job Number from JobRun: %s'%(self.count))
            print("Job Name", self.jobrun[-1])
            print("Extract_regex", self.jobrun[-1][20])
            if len(self.jobrun[-1]) > 0:
                self.update_job_runtime(self.jobrun[-1][0], 1)
                print("Running job %s, %s\n"%(self.jobrun[-1][0],
                                              self.jobrun[-1][3]))
                request_response = self.get_url(self.jobrun[-1][3])
                    
                if self.depth < 1:
                    link_data = request_response.links
                    print('Job links Found:\n %s'%(link_data,))
                    self.depth += 1
                    for link_count in range(len(link_data)):        
                        extension_count = 0
                        extension_pattern = ['.ico', '.css', '.jpg', '.png',
                                             '.svg', '.gif', 'login', 'blog',
                                             'donate', '.js', 'thumbnail']
                        for extension in extension_pattern:
                                if re.search(extension, link_url):
                                    extension_count += 1
                            
                        if extension_count == 0:
                                
                            if link_url in self.url_list:
                                pass
                            else:
                                self.url_list.append(link_url)
                                job_2 = job
                                job2[13] = '0'
                                job2[3] = link_url
                                self.process_job(job_2)
                        
                    self.depth = 0
                    
                page_content = request_response.text
                url_page_list = page_content.splitlines()
                #This is a hack for the XML style jobs that fetch large amounts of proxies
                try:
                    if len(url_page_list)==1:
                        url_page_list = re.split(pattern=':|/', string=page_content)
                    else:
                        pass
                except:
                    pass
                #This is a hack for job 68 Bruteforcers, job 119 proxz, and job 166 newdailyproxy
                try:
                    if job[0] in [68, 119, 166]:
                        url_page_list = re.split(pattern='</td>|ip>|<br', string=page_content)
                    else:
                        pass
                except:
                    pass
                for line_count in range(len(url_page_list)):
                        
                    print('*********')
                    e_regex = self.jobrun[-1][20]
                    print('Fetched e_regex: %s'%(e_regex))
                    page_line = url_page_list[line_count]
                    print(page_line)
                    print('*********')
                    if not page_line == '':
                        if re.search(e_regex, page_line):
                            print("**Matched line**", page_line)
                            page_line = re.search(e_regex, page_line).group()
                            print("**Matched value**", page_line) 
                            exclude_regex = self.jobrun[-1][9]
                            if exclude_regex > 0:
                                ex_regex = self.jobrun[-1][24]
                                print('*********')
                                print('Fetched ex_regex: %s'%(ex_regex))
                                print(page_line)
                                if re.search(ex_regex, page_line):
                                    page_line=re.sub(ex_regex, '', page_line)
                                    print('New page line:\n %s'%(page_line))
                                    continue
                                
                            cleanup_regex = self.jobrun[-1][10]
                            if cleanup_regex > 0:
                                c_regex = self.jobrun[-1][28]
                                print('Fetched c_regex: %s'%(c_regex))
                                print('*********')
                                page_line=re.sub(c_regex, '', page_line)
                                print('New page line:\n %s'%(page_line))
                                
                            self.unique_content[page_line] = {}
                            extract_type = self.jobrun[-1][6]
                            self.unique_content[page_line]['type'] = extract_type
                            job_source = self.jobrun[-1][2]
                            self.unique_content[page_line]['source'] = job_source
                                
                            if 'job_key' not in self.unique_content[page_line]:
                                fetched_job_key = self.jobrun[-1][0]
                                self.unique_content[page_line]['job_key'] = fetched_job_key
                            extract_class = self.jobrun[-1][7]
                            self.unique_content[page_line]['extract_class'] = extract_class
                            #print('*****')
                            #print(self.unique_content)
                            #print('*****')
                        
                        else:
                            print('Skipping as Regex not matched')
                
                else:
                    print('Skipping Job as content not found')
                
            self.process_content(self.unique_content)
    
    def process_stat(self, source_d):
        """
        @Description: Method to updates data in data_audit_stats
        Parameters:
                source_d = source directory
        Perl Method: This definition covers perl method processStat
        """
        
        data_stat_query = "select das_key, count from data_audit_stats where \
        data_key=%s and time_interval=%s and timeslice=%s \
        and source='%s';"%(source_d['data_key'], source_d['time_interval'],
                         source_d['timeslice'], source_d['source'])
        
        data_stat_tuple = self.db_fetch_query_execute(data_stat_query)
        if len(data_stat_tuple) > 0:
            if source_d['count'] != data_stat_tuple[0]['count']:
                
                update_data_stat_query = "update data_audit_stats \
                    set data_key=%s, time_interval=%s, timeslice=%s, \
                    source='%s', count=%s where das_key=%s"%(source_d['data_key'], 
                                                           source_d['time_interval'],
                                                           source_d['timeslice'], 
                                                           source_d['source'],
                                                           source_d['count'],
                                                           data_stat_tuple[0]["das_key"])
                query_status = self.db_update_query_execute(update_data_stat_query)
            return data_stat_tuple[0]["das_key"]
        
        else:
            insert_data_stat_query = "insert into data_audit_stats \
            (data_key, time_interval, timeslice, source, count) values \
                (%s, %s, %s, '%s', %s);"%(source_d['data_key'],
                                        source_d['time_interval'],
                                        source_d['timeslice'], source_d['source'],
                                        source_d['count'])
            
            self.db_update_query_execute(insert_data_stat_query)
            data_response = self.process_stat(source_d)
            return data_response
        
    def who_is(self, ip, key):
        """
        @Description: Method to get details of IP and pass data
                        to enrich method
        Parameters:
                ip = ip whose details to be fetched
                key = key to insert data
        Perl Method: This definition covers perl method whois
        """
        
        try:
            ip_obj = IPWhois(ip)
            ip_dict = ip_obj.lookup_whois()
            ip_dict_keys = ip_dict.keys()
            for key_count in range(len(ip_dict)):
                self.enrich(key, ip_dict[ip_dict_keys[key_count]])
        except:
            raise SystemExit("Error occurred while fetching details of IP")
    
    def who_is_query(self, ip, key):
        """
        @Description: Method to get whois details of IP and pass data to the metadata table
        Parameters:
             ip = IP address
             key = data_key
        """

        try:
            ip_obj = IPWhois(ip)
            ip_dict = ip_obj.lookup_whois()
            whois_asn = ip_dict['asn']
            whois_asn_country = ip_dict['asn_country_code']
            whois_address = ip_dict['nets'][0]['address']
            whois_cidr = ip_dict['nets'][0]['cidr']
            whois_country = ip_dict['nets'][0]['country']
            whois_last_update = ip_dict['nets'][0]['updated']
            whois_name = ip_dict['nets'][0]['name']
            whois_description = ip_dict['nets'][0]['description'][:1000]
            whois_handle = ip_dict['nets'][0]['handle']
            whois_email = ip_dict['nets'][0]['emails'][0]

            self.insert_metadata(key, "Whois_asn", whois_asn)
            self.insert_metadata(key, "Whois_asn_country", whois_asn_country)
            self.insert_metadata(key, "Whois_address", whois_address)
            self.insert_metadata(key, "Whois_cidr", whois_cidr)
            self.insert_metadata(key, "Whois_country", whois_country)
            self.insert_metadata(key, "Whois_update", whois_last_update)
            self.insert_metadata(key, "Whois_name", whois_name)
            self.insert_metadata(key, "Whois_description", whois_description)
            self.insert_metadata(key, "Whois_handle", whois_handle)
            self.insert_metadata(key, "Whois_email", whois_email)
        except:
            raise SystemExit("Error occurred while fetching details of IP")

    def enrich(self, key, data):
        """
        @Description: Method to insert data in data_enrichment table
        Parameters:
                key = key
                data = data to be inserted
        Perl Method: This definition covers perl method enrich
        """
        
        regex_query = 'select * from data_enrichment_regex;'
        regex_list = self.db_fetch_query_execute(regex_query)
        for regex_count in range(len(regex_list)):
            if re.search(r'/$der->[$l]->{der_value}/i', data):
                self.insert_data_enrichment(key, regex_list[regex_count]['der_key'])
    
    def log_geo_metaData(self, data_val, data_key):
        """
        @Description: Method to fetch data from maxmind file
        and invokes insert metadata method
        Parameters:
                data_key = key
                data_val = value 
        Perl Method: This definition covers perl method logGeoMetadata
        """
        
        try:
            database_record = self.geoip_database.city(data_val)
            database_record_country = database_record.country.name
            database_record_city = database_record.city.name
            database_record_latitude = database_record.location.latitude
            database_record_longitude = database_record.location.longitude
            
            self.insert_metadata(data_key, "Country_Name", database_record_country)
            self.insert_metadata(data_key, "City", database_record_city)
            self.insert_metadata(data_key, "Latitude", database_record_latitude)
            self.insert_metadata(data_key, "Longitude", database_record_longitude)
        
        except:
            raise SystemExit("Error occurred while fetching data from maxmind file.")

    def log_isp_org_metadata(self, data_val, data_key):
        """Description: Method to fetch data from maxmind ISP file and invoke insert metadata method
        Parameters:
              data_key=key
              data_val = value
        """
        try:
            database_record = self.geoip_database.isp(data_val)
            database_record_isp = database_record.isp
            database_record_org = database_record.organization
            database_record_asn = database_record.autonomous_system_number
            database_record_asn_name = database_record.autonomous_system_organization

            self.insert_metadata(data_key, "ISP", database_record_isp)
            self.insert_metadata(data_key, "Organization", database_record_org)
            self.insert_metadata(data_key, "ASN", database_record_asn)
            self.insert_metadata(data_key, "ASN_Name", database_record_asn_name)
        except:
            print("Error occured while fetching data from maxmind file.") 
            raise 

