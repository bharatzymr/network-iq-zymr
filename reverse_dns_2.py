#!/usr/bin/python

from nwiq import nwiq

class reverse_dns_2:

    def __init__(self):
        """
            @Description: Initialize the class and define objects
        """
        
        print("Instantiate Class")
        self.ins_nwiq = nwiq()
        print("Instantiate www definition")
        self.ins_nwiq.www()
        
    def reverse_dns_test(self):
        
        self.ins_nwiq.db_connect("nwiq-python.cbmwsbw6g8jt.us-west-2.rds.amazonaws.com", "attack-job-server", "root", "IYHevDPLuvYMTJ2G")
        
        for loop_count in range(100):
            print("Executing select query for %s round"%(loop_count+1))
            db_query_1 = "select data_key from data where creation_date > \
            date_sub(now(), interval 3 hour) ORDER BY RAND() limit 1;"
            
            data_tuple_1 = self.ins_nwiq.db_fetch_query_execute(db_query_1)
            print(data_tuple_1)
            
            if len(data_tuple_1) > 0:
                print(data_tuple_1[0][0])
                db_query_2 = "select * from data where data_key=%s and \
                data_type='ip_v4';"%(data_tuple_1[0][0])
                
                data_tuple_2 = self.ins_nwiq.db_fetch_query_execute(db_query_2)
                print(data_tuple_2)
                if len(data_tuple_2) > 0:
                    
                        data_value = data_tuple_2[0][3]
                        print(data_value)
                        target_ip = '.'.join(reversed(data_value.split("."))) + '.in-addr.arpa'
                        print(target_ip)
                        try:
                            dns_response = self.ins_nwiq.dns_obj.query('%s'%(target_ip), 'PTR')
                            print(dns_response)
                            if dns_response:
                                for rdata in dns_response:
                                    print(rdata)
                                    response_domain = rdata
                                    print(response_domain)
                        except:
                            print("Error occurred while fetching reverse dns content \
                            so inserting IP as domain in metadata")
                            response_domain = data_value
                        
                        self.ins_nwiq.insert_metadata(data_tuple_1[0][0],
                                                      'domain', response_domain)
                
            else:
                print("Select query returned empty tuple for %s round..."%(loop_count))
    
if __name__ == "__main__":
    ins_reverse_dns = reverse_dns_2()
    ins_reverse_dns.reverse_dns_test()
