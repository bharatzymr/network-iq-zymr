#!/bin/bash
set -e 
echo `date` > /tmp/ziplog.txt
python3 /home/ubuntu/network-iq-zymr/create_ip_lists.py
echo "Produced Lists" >> /tmp/ziplog.txt
cd /home/ubuntu/attack-db/lists
cat *.txt > nwiqv3.txt
echo "Cat all list filed together into txt file" >> /tmp/ziplog.txt
python3 /home/ubuntu/network-iq-zymr/nwiqlist-json.py
echo "Produced Json" >> /tmp/ziplog.txt
python3 /home/ubuntu/network-iq-zymr/create_isp_lists.py
echo "Produced ISP Lists" >> /tmp/ziplog.txt
python3 /home/ubuntu/network-iq-zymr/create_ua_lists.py
echo "Produced User Agent Lists" >> /tmp/ziplog.txt
s3cmd sync --no-preserve s3://xangent-packages/build/polygraph-engine/develop/ /home/ubuntu/temp
echo "Downloaded polygraph to temp file" >> /tmp/ziplog.txt
cd /home/ubuntu/temp
tar xvfz polygraph-engine.tgz
java -cp polygraph-engine/jar/polygraphengine-complete-1.0-SNAPSHOT.jar com.xangent.geoip.TestNetworkIQFeedsFile /home/ubuntu/attack-db/lists/nwiqv3.txt /home/ubuntu/attack-db/nwiqv3_isp_org_list.txt /home/ubuntu/attack-db/nwiqv3_user_agent_list.txt 
if [ $? -ne 0 ]; then
	echo `date` "Failure testing text file" >> /tmp/ziplog.txt
	exit 1
fi
java -cp polygraph-engine/jar/polygraphengine-complete-1.0-SNAPSHOT.jar com.xangent.geoip.TestNetworkIQFeedsFile /home/ubuntu/attack-db/lists/Proxies-level-1.txt /home/ubuntu/attack-db/nwiqv3_isp_org_list.txt /home/ubuntu/attack-db/nwiqv3_user_agent_list.txt
if [ $? -ne 0 ]; then
	echo `date` "Failure testing proxy list file" >> /tmp/ziplog.txt
	exit 1
fi
s3cmd put /home/ubuntu/attack-db/lists/Proxies-level-1.txt s3://xangent-filestore/networkiq/proxylistv3.txt
echo "Put the proxy list into S3 nwiq feed bucket" >> /tmp/ziplog.txt
s3cmd put /home/ubuntu/attack-db/lists/nwiqv3.json s3://xangent-filestore/networkiq/nwiqv3.json
s3cmd put /home/ubuntu/attack-db/lists/nwiqv3.txt s3://xangent-filestore/networkiq/nwiqv3.txt
s3cmd put /home/ubuntu/attack-db/nwiqv3_isp_org_list.txt s3://xangent-filestore/networkiq/nwiqv3_isp_org_list.txt
s3cmd put /home/ubuntu/attack-db/nwiqv3_user_agent_list.txt s3://xangent-filestore/networkiq/nwiqv3_user_agent_list.txt
echo "Put all 4 files into S3 filestore" >> /tmp/ziplog.txt
mv /home/ubuntu/attack-db/lists/nwiqv3.json /home/ubuntu/temp/system/nwiqv3.json
mv /home/ubuntu/attack-db/lists/nwiqv3.txt /home/ubuntu/temp/system/nwiqv3.txt
mv /home/ubuntu/attack-db/nwiqv3_isp_org_list.txt /home/ubuntu/temp/system/nwiqv3_isp_org_list.txt
mv /home/ubuntu/attack-db/nwiqv3_user_agent_list.txt /home/ubuntu/temp/system/nwiqv3_user_agent_list.txt
cd /home/ubuntu/temp
zip -r nwiqv3.zip ./system
echo "Zipped the system directory" >> /tmp/ziplog.txt
s3cmd put /home/ubuntu/temp/nwiqv3.zip s3://networkiq/feeds/nwiq-v3.zip
echo `date` "Successfully put zip file to S3 nwiq feed bucket" >> /tmp/ziplog.txt

